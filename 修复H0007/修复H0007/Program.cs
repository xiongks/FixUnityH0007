﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace 修复H0007
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!File.Exists("hasp_update.exe"))
            {
                Console.WriteLine("请将此程序放在Unity安装路径的Editor路径下再运行!");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("正在删除:\"C:\\ProgramData\\SafeNet Sentinel\\Sentinel LDK\"");
            string output = ExcuteCMD("rd /s /q \"C:\\ProgramData\\SafeNet Sentinel\\Sentinel LDK\"");
            Console.WriteLine(output);

            Console.WriteLine("正在执行命令:hasp_update.exe u unity-sl.v2c");
             output = ExcuteCMD("hasp_update.exe u unity-sl.v2c");
            Console.WriteLine(output);

            Console.WriteLine("执行完毕,请检查Untiy是否正能正常打开");
            Console.WriteLine("学游戏开发找熊老师 QQ:506994768");
            Console.ReadLine();
        }

        private static string ExcuteCMD(string str)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = true;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str + "&exit");

            p.StandardInput.AutoFlush = true;
            //p.StandardInput.WriteLine("exit");
            //向标准输入写入要执行的命令。这里使用&是批处理命令的符号，表示前面一个命令不管是否执行成功都执行后面(exit)命令，如果不执行exit命令，后面调用ReadToEnd()方法会假死
            //同类的符号还有&&和||前者表示必须前一个命令执行成功才会执行后面的命令，后者表示必须前一个命令执行失败才会执行后面的命令



            //获取cmd窗口的输出信息
            string output = p.StandardOutput.ReadToEnd();

            //StreamReader reader = p.StandardOutput;
            //string line=reader.ReadLine();
            //while (!reader.EndOfStream)
            //{
            //    str += line + "  ";
            //    line = reader.ReadLine();
            //}

            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
            return output;
        }
    }
}
